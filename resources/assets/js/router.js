import Vue from 'vue'
import VueRouter from 'vue-router'

import IndexLayout from './components/layouts/IndexLayout'
import DashboardLayout from './components/layouts/DashboardLayout'
import LoginPage from './components/views/LoginPage'
import DashboardPage from './components/views/DashboardPage'
import UsersPage from './components/views/UsersPage'
import DepartmentsPage from './components/views/DepartmentsPage'
import TasksPage from './components/views/TasksPage'
import DevicesPage from './components/views/DevicesPage'
import TicketsPage from './components/views/TicketsPage'

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
        path: '/',
        component: IndexLayout,
        meta: { requiresAuth: false },
        children: [
        	{
        		path: '/',
        		name: 'LoginPage',
        		component: LoginPage
        	}
        ]
    },
    {
    	path: '/dashboard',
    	component: DashboardLayout,
        meta: { requiresAuth: true },
    	children: [
    		{
    			path: '',
    			name: 'DashboardPage',
    			component: DashboardPage
    		},
    		{
    			path: 'users',
    			name: 'UsersPage',
    			component: UsersPage
    		},
    		{
    			path: 'departments',
    			name: 'DepartmentsPage',
    			component: DepartmentsPage
    		},
    		{
    			path: 'tasks',
    			name: 'TasksPage',
    			component: TasksPage
    		},
    		{
    			path: 'devices',
    			name: 'DevicesPage',
    			component: DevicesPage
    		},
    		{
    			path: 'tickets',
    			name: 'TicketsPage',
    			component: TicketsPage
    		}
    	]

    }
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    Vue.http.get('/api/v1/user')
    .then(res => {
        next()
    }, e => {
        next({
            name: 'LoginPage'
        })
    })
  } else {
    Vue.http.get('/api/v1/user')
    .then(res => {
        next({
            name: 'DashboardPage'
        })
    }, e => {
        next()
    })
  }
})

export { router }