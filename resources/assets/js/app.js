import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueLocalStorage from 'vue-localstorage'
import { router } from './router'
import Antd from 'vue-antd-ui'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'vue-antd-ui/dist/antd.css'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueLocalStorage)
Vue.use(Antd)

import App from './components/App'

export const VmStore = {
	oauth: {
		grant_type: 'password',
		client_id: 3,
		client_secret: 'iNBYL07O4aNvIIXwh7HHOkKVS9vNFdeclAsDRTdb',
		scopes: '*',
	}
}

Vue.http.interceptors.push(function(request) {
  request.headers.set('Authorization', 'Bearer '+Vue.localStorage.get('access_token'));
});

const app = new Vue({
  el: '#app',
  components: { App },
  router,
});