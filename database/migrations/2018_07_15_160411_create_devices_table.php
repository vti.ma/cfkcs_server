<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('device_type_id');
            $table->string('description');
            $table->integer('device_status_id');
            $table->integer('owner_id');
            $table->string('loc_building')->nullable();
            $table->integer('loc_floor')->nullable();
            $table->string('loc_room')->nullable();
            $table->text('comment')->nullable();
            $table->string('net_name')->nullable();
            $table->string('ip')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('inventory_number')->nullable();
            $table->json('documents')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
