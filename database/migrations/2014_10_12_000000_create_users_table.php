<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('f_name'); //Имя
            $table->string('l_name'); //Фамилия
            $table->string('m_name')->nullable(); //Отчество
            $table->string('full_name');
            $table->string('mob_phone')->nullable();
            $table->string('work_phone')->nullable();
            //$table->integer('group_id')->default(0);
            $table->json('scopes')->nullable();
            $table->integer('department_id')->default(0);
            $table->string('position')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
