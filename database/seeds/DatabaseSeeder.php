<?php

use Illuminate\Database\Seeder;
use App\Task;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
	    $task = new Task();
	    $task->title = 'Task';
	    $task->description = 'Task description';
	    $task->owner_id = 1;
	    $task->responsible_id = 18;
	    $task->status = 'success';
	    $task->save();
    }
}
