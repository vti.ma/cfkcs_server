<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    private $scopes;
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();

        Gate::define('viewUsers', function ($user) {
            return $this->getScope('viewUsers'); 
        });
    }

    private function getScope($scope)
    {
        $scopes = collect(json_decode(Auth::user()->scopes));
        return $scopes->contains($scope)
        || $scopes->contains('*');
    }
}
