<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    protected $fillable = [
        'owner_id', 'path'
    ];

    public function owner()
    {
    	return $this->belongsTo('App\User', 'avatar_id', 'id');
    }
}
