<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department;

class addDepartment extends Controller
{
    public function __invoke(Request $request)
    {
    	$department = Department::where('name', $request->name)->first();
    	if ($department) {
    		return abort(422);
    	} else {
    		return Department::create([
	    		'name' => $request->name,
	    		'description' => $request->description,
	    	]);
    	}
    }
}
