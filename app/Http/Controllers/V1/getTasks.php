<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Task;

class getTasks extends Controller
{
    public function __invoke(Request $request)
    {
    	return Task::where([
    		['created_at', '>=', $request->firstDate],
    		['created_at', '<=', $request->lastDate],
    	])->get();
    }

    public function date($date)
    {
    	return Task::where('created_at', $date)->get();
    }
}
