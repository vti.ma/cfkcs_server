<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class getUser extends Controller
{
    public function __invoke($id)
    {
    	return User::with('department')->find($id);
    }
}
