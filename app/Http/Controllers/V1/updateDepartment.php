<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department;

class updateDepartment extends Controller
{
    public function __invoke(Request $request)
    {
    	$department = Department::find($request->id);
    	$department->name = $request->name;
    	$department->description = $request->description;
    	$department->save();
    	return 'ok';
    }
}
