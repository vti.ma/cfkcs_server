<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Avatar;
use App\User;
use Storage;

class updateAvatar extends Controller
{
    public function __invoke($id, Request $request)
    {
    	$user = User::find($id);
    	$path = $request->file('avatar')->store('avatars');
        Storage::delete($user->avatar);
    	$user->avatar = $path;
    	$user->save();
    	return 'ok';
    }
}
