<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class updateUser extends Controller
{
    public function __invoke(Request $request)
    {
    	$user = User::find($request->id);
        $user->email = $request->email;
    	$user->work_email = $request->work_email;
    	$user->f_name = $request->f_name;
    	$user->l_name = $request->l_name;
    	$user->m_name = $request->m_name;
    	$user->full_name = $request->l_name.' '.$request->f_name.' '.$request->m_name;
    	$user->mob_phone = $request->mob_phone;
    	$user->work_phone = $request->work_phone;
    	$user->department_id = $request->department;
        $user->position = $request->position;

    	$user->save();
    	return 'ok';
    }
}
