<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class updatePassword extends Controller
{
    public function __invoke(Request $request)
    {
    	$user = User::find($request->id);
    	$user->password = Hash::make($request->password);
    	$user->save();
    	return 'ok';
    }
}
