<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department;

class getAllDepartments extends Controller
{
    public function __invoke()
    {
    	return Department::with('users')->get();
    }
}
