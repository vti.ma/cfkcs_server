<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department;

class getDepartment extends Controller
{
    public function __invoke($id)
    {
    	return Department::find($id);
    }
}
