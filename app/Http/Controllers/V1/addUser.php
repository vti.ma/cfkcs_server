<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class addUser extends Controller
{
    public function __invoke(Request $request)
    {
    	$user = User::where('email', $request->email)->first();
    	if ($user) {
    		abort(422);
    	} else {
    		return User::create([
	    		'f_name' => $request->f_name,
	    		'l_name' => $request->l_name,
	    		'm_name' => $request->m_name,
	    		'full_name' => $request->f_name.' '.$request->l_name.' '.$request->m_name,
	    		'email' => $request->email,
	    		'position' => $request->position,
	    		'department_id' => $request->department,
	    		'mob_phone' => $request->mob_phone,
	    		'work_phone' => $request->work_phone,
	    		'work_email' => $request->work_email,
	    		'password' => Hash::make($request->password),
	    	]);
    	}
    }
}
