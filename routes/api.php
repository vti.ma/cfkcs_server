<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1'], function () {
	Route::middleware('auth:api')->get('/user', function (Request $request) {return $request->user();});  
	Route::group(['namespace' => 'V1'], function() {
	
		Route::middleware('auth:api')->get('/getAllUsers', 'getAllUsers');
		Route::middleware('auth:api')->get('/getAllDepartments', 'getAllDepartments');
		Route::middleware('auth:api')->post('/addDepartment', 'addDepartment');
		Route::middleware('auth:api')->post('/addUser', 'addUser');
		Route::middleware('auth:api')->get('/getUser/{id}', 'getUser');
		Route::middleware('auth:api')->post('/updateUser', 'updateUser');
		Route::middleware('auth:api')->get('/getDepartment/{id}', 'getDepartment');
		Route::middleware('auth:api')->post('/updateDepartment', 'updateDepartment');
		Route::middleware('auth:api')->post('/updatePassword', 'updatePassword');
		Route::post('/updateAvatar/{id}', 'updateAvatar');

		Route::middleware('auth:api')->post('/getTasks', 'getTasks');
		Route::middleware('auth:api')->get('/getTasks/{date}', 'getTasks@date');

	});

});